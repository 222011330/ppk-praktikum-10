Nama : Arnoldy Fatwa Rahmadin

NIM : 222011330

Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 10

1. Kegiatan 10.4

    - Tampilan kedua button saat belum di klik
    ![ss_awal](Dokumentasi/SS1.jpg)

    - Tampilan saat Button 1 di klik
    ![ss_btn1](Dokumentasi/SS_button1.jpg)

    - Tampilan saat Button 2 di klik
    ![ss_btn2](Dokumentasi/SS_button2.jpg)

2. Aplikasi Kalkulator Luas Persegi

    - Tampilan Awal Aplikasi Kalkulator Luas Persegi
    ![ss_btn2](Dokumentasi/SS_Kalkulator.jpg)

    - Tampilan ketika dilakukan input sisi persegi kemudian klik button Hitung
    ![ss_btn2](Dokumentasi/SS_Hitung.jpg)

    - Tampilan ketika klik button Refresh, maka akan mereset inputan sisi persegi
    ![ss_btn2](Dokumentasi/SS_Refresh.jpg)
